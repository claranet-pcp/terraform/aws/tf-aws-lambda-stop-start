import boto3
import os

def toggle_instance(instance, start):
    ec2 = boto3.resource("ec2", region_name=os.environ['region'])
    instance = ec2.Instance(instance["InstanceId"])
    if start:
        instance.start()
    else:
        instance.stop()

def toggle_instances(start):
    ec2_client = boto3.client("ec2", region_name=os.environ['region'])
    action_name = 'start' if start else 'stop'
    print(f"Looking for instances tagged {os.environ['instance_tag']} to {action_name}")
    # Look for the tag Scheduled-Stop-Start
    description = ec2_client.describe_instances(
        Filters=[
            { 'Name': 'instance-state-name', 'Values': ['stopped' if start else 'running'] },
            { 'Name': f"tag:{os.environ['instance_tag']}", 'Values' : ['yes']}
        ]
    )

    for reservation in description["Reservations"]:
        for instance in reservation["Instances"]:
            print(f"instance to {action_name}: {instance['InstanceId']}")
            toggle_instance(instance, start)

def lambda_handler(event, context):
    toggle_instances("start" in event["resources"][0])
