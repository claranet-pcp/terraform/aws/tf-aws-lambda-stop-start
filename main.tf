locals {
  name_prefix   = "${var.name}-${var.envname}"
  function_name = "${local.name_prefix}-stop-start-schedule"
}

## create lambda package
data "archive_file" "create_lambda_package" {
  count       = var.enabled ? 1 : 0
  type        = "zip"
  source_dir  = "${path.module}/include"
  output_path = ".terraform/stop-start.zip"
}

data "aws_iam_policy_document" "lambda" {
  count = var.enabled ? 1 : 0

  statement {
    sid       = "CloudwatchLogs"
    resources = [
      "arn:aws:logs:*:*:log-group:/aws/lambda/${local.function_name}",
      "arn:aws:logs:*:*:log-group:/aws/lambda/${local.function_name}:*"
    ]
    actions   = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
  }

  statement {
    sid       = "ec2Describe"
    resources = ["*"]
    actions   = ["ec2:DescribeInstances"]
  }

  statement {
    sid       = "ec2StopStart"
    resources = ["arn:aws:ec2:${var.region}:*:instance/*"]
    actions   = [
      "ec2:StartInstances",
      "ec2:StopInstances"
    ]

    condition {
      test     = "StringEquals"
      variable = "aws:ResourceTag/${var.instance_tag}"
      values   = ["yes"]
    }
  }
}

resource "aws_iam_role_policy" "lambda" {
  count  = var.enabled ? 1 : 0
  name   = "lambda_role_policy"
  role   = aws_iam_role.lambda[0].id
  policy = data.aws_iam_policy_document.lambda[0].json
}

resource "random_string" "lambda_role" {
  length = 8
  special = false
}
resource "aws_iam_role" "lambda" {
  count = var.enabled ? 1 : 0
  name  = "${var.name}-${var.envname}-lambda-role-${random_string.lambda_role.result}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]

}
EOF
}

resource "aws_lambda_function" "stop_start_lambda" {
  count            = var.enabled ? 1 : 0
  filename         = ".terraform/stop-start.zip"
  source_code_hash = data.archive_file.create_lambda_package[0].output_base64sha256
  function_name    = local.function_name
  role             = aws_iam_role.lambda[0].arn
  handler          = "start-stop.lambda_handler"
  runtime          = "python3.8"
  timeout          = 300

  environment {
    variables = {
      region       = var.region
      instance_tag = var.instance_tag
    }
  }
}

resource "aws_cloudwatch_event_rule" "ec2_start" {
  count               = var.enabled ? 1 : 0
  name                = "${local.name_prefix}-instance-start"
  description         = "Capture running, stopped or terminated"
  schedule_expression = var.cron_start_schedule
}

resource "aws_cloudwatch_event_rule" "ec2_stop" {
  count               = var.enabled ? 1 : 0
  name                = "${local.name_prefix}-instance-stop"
  description         = "Capture running, stopped or terminated"
  schedule_expression = var.cron_stop_schedule
}

resource "aws_cloudwatch_event_target" "ec2_start" {
  count = var.enabled ? 1 : 0
  rule  = aws_cloudwatch_event_rule.ec2_start[0].name
  arn   = aws_lambda_function.stop_start_lambda[0].arn
}

resource "aws_cloudwatch_event_target" "ec2_stop" {
  count = var.enabled ? 1 : 0
  rule  = aws_cloudwatch_event_rule.ec2_stop[0].name
  arn   = aws_lambda_function.stop_start_lambda[0].arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_ec2_start" {
  count         = var.enabled ? 1 : 0
  statement_id  = "AllowExecutionFromCloudWatchstart"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.stop_start_lambda[0].function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ec2_start[0].arn
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_ec2_stop" {
  count         = var.enabled ? 1 : 0
  statement_id  = "AllowExecutionFromCloudWatchstop"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.stop_start_lambda[0].function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.ec2_stop[0].arn
}
